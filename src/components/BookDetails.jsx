import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import axios from "axios";

export default function BookDetails() {
  const { bookKey } = useParams();
  const [bookDetails, setBookDetails] = useState([]);

  useEffect(() => {
    axios
      .get(`https://openlibrary.org/works/${bookKey}.json`)
      .then((response) => {
        console.log(response);
        setBookDetails(response?.data);
      });
  }, []);

  return (
    <div>
      <Link to="/">Back to books list</Link>
      <h2>Book Details</h2>
      <h3>{bookDetails.title}</h3>
      <p>{bookDetails.description}</p>
    </div>
  );
}
