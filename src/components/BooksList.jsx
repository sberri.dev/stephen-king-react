import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import "../components/BooksList.css";
import defaultImage from "../defaultImage.jpeg"

export default function BooksList() {
  const [books, setBooks] = useState([]);

  useEffect(() => {
    axios
      .get(`https://openlibrary.org/authors/OL2162284A/works.json`)
      .then((response) => setBooks(response?.data.entries));
  }, []);

  // déclaration de l'image par défaut en cas d'erreur d'affichage des couverture
  const handleImageError = (event) => {
    event.target.src = defaultImage; 
  };


  return (
    <div>
      <h1>Stephen King collection books</h1>
      <div className="container-book-card">
      <ul className="book-list">
        {books.map((book) => (
          <li className="each-book" key={book.key}>
            <Link className="book-title" to={"/bookdetails" + book.key}>{book.title}</Link>
            {book.covers && (
              <img className="cover-book"
              // Index [0] afin qu'il affiche que la premiere couverture de l'api 
                src={`https://covers.openlibrary.org/b/id/${book.covers[0]}-M.jpg`}
                alt={`Book's cover`}
                onError={handleImageError} // En cas d'erreur d'image, une image par défaut s'affichera
              />
            )}
          </li>
        ))}
      </ul>
      </div>
    </div>
  );
}
