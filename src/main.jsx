import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  Link,
} from "react-router-dom";
import BooksList from "./components/BooksList.jsx";
import BookDetails from "./components/BookDetails.jsx";

//Creation de router pour les 2 composants
const router = createBrowserRouter([
  {
    path: "/",
    element: <BooksList />
  },
  {
    path: "/bookdetails/works/:bookKey",
    element: <BookDetails />
  },
]);


ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
     <RouterProvider router={router} />
  </React.StrictMode>
);
